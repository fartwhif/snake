#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <time.h>
#include <unistd.h>
#include <stdarg.h>

#ifdef WINDOWS
#include <windows.h>
#include <conio.h>
#else
#include <stdio.h>
#include <sys/select.h>
#include <termios.h>
#include <sys/ioctl.h>
#endif

///https://stackoverflow.com/a/33412960/6620171
int nsleep(long miliseconds)
{
    struct timespec req, rem;

    if(miliseconds > 999)
    {
        req.tv_sec = (int)(miliseconds / 1000);                            /* Must be Non-Negative */
        req.tv_nsec = (miliseconds - ((long)req.tv_sec * 1000)) * 1000000; /* Must be in range of 0 to 999999999 */
    }
    else
    {
        req.tv_sec = 0;                         /* Must be Non-Negative */
        req.tv_nsec = miliseconds * 1000000;    /* Must be in range of 0 to 999999999 */
    }
    return nanosleep(&req, &rem);
}

#ifdef WINDOWS
void printToCoordinates(int x, int y, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
    fflush(stdout);
}
///https://stackoverflow.com/a/30126700/6620171
void hidecursor()
{
    HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO info;
    info.dwSize = 100;
    info.bVisible = FALSE;
    SetConsoleCursorInfo(consoleHandle, &info);
}
int get_input()
{
    int key = -1;
    while (kbhit() != 0)
    {
        key = getch();
    }
    return key;
}
#define UP 72
#define DN 80
#define LF 75
#define RT 77
#define W  119
#define S  115
#define A  97
#define D  100
#define SB 32
#define F5 63
#define TERMINAL_OFFSET_X 0
#define TERMINAL_OFFSET_Y 0
#define FIELD_OFFSET_X    0
#define FIELD_OFFSET_X1   0
#define FIELD_OFFSET_Y    0
#define FIELD_OFFSET_Y1   0
#define FIELD_OFFSET_Y2   0
#else
char getch(void)
{
    //assumes canonical mode terminal setting is OFF
    char buf = 0;
    fflush(stdout);
    if(read(0, &buf, 1) < 0)
        perror("read()");
    return buf;
}
/**
 Linux (POSIX) implementation of _kbhit().
 Morgan McGuire, morgan@cs.brown.edu
 */
int _kbhit()
{
    static const int STDIN = 0;
    static bool initialized = false;

    if (! initialized)
    {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON; //CANONICAL OFF
        term.c_lflag &= ~ECHO;   //ECHO OFF
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}
int get_input()
{
    int key = -1;
    while (_kbhit() != 0)
    {
        key = getch();
    }
    return key;
}
///https://stackoverflow.com/a/1670910/6620171
void printToCoordinates(int x, int y, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    printf("\033[%d;%dH", y, x);
    vprintf(format, args);
    va_end(args);
    fflush(stdout);
}
///https://stackoverflow.com/a/55313602/6620171
void hidecursor()
{
    printf("\e[?25l");
}
#define UP 65
#define DN 66
#define LF 68
#define RT 67
#define W  119
#define S  115
#define A  97
#define D  100
#define SB 32
#define F5 126
#define TERMINAL_OFFSET_X 1
#define TERMINAL_OFFSET_Y 1
#define FIELD_OFFSET_X    1
#define FIELD_OFFSET_X1   2
#define FIELD_OFFSET_Y    1
#define FIELD_OFFSET_Y1   2
#define FIELD_OFFSET_Y2   3
#endif

#endif // UTIL_H_INCLUDED
