#ifndef FOODPARTICLE_H_INCLUDED
#define FOODPARTICLE_H_INCLUDED

class FoodParticle
{
public:
    int X; //X location in the field
    int Y; //Y location in the field
    int nutrition; //the amount of length this particle will cause the snake to grow
};

#endif // FOODPARTICLE_H_INCLUDED
