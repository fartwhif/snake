#ifndef BODYSEGMENT_H_INCLUDED
#define BODYSEGMENT_H_INCLUDED
class BodySegment
{
public:
    int X;
    int Y;
    struct BodySegment* next;
    struct BodySegment* prev;
	void push(struct BodySegment** head_ref, int X, int Y);
	void insertAfter(struct BodySegment* prev_node, int X, int Y);
	void append(struct BodySegment** head_ref, int X, int Y);
	void Destroy(struct BodySegment** head_ref);
	BodySegment* DestroyTail(struct BodySegment** head_ref);
	BodySegment* GetTail(struct BodySegment** head_ref);
	BodySegment* GetHead(struct BodySegment** head_ref);
	bool HitTest(struct BodySegment** head_ref, int X, int Y, bool self);
};
#endif // BODYSEGMENT_H_INCLUDED
