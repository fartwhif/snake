#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include "Util.h"
#include "BodySegment.h"
#include "FoodParticle.h"
using namespace std;

enum Direction
{
    Right,
    Down,
    Left,
    Up
};

bool sound = false;
bool drawFood = false;
bool moarFood = false;
bool boom = false; //game over animation is in progress
bool in_play = false; //whether or not the game is active
int field_width = 50 + TERMINAL_OFFSET_X; //width of the terminal
int field_height = 25 + TERMINAL_OFFSET_Y; //height of the terminal
int length = 10; //number of snake segments
int key = -1; //keyboard key
int prevkey = -1; //keyboard key
int growth_remaining = 4; //length to be added to the snake over frames
int food_nutrition = 4; //the amount of length the next particle will cause the snake to grow
long speed = 100; //time to sleep between frames
Direction dir = Direction::Right; //direction of the snake
struct BodySegment* snake = NULL; //the snake
struct BodySegment* snake_head = NULL; //the snake
struct BodySegment* snake_tail = NULL; //the snake
struct FoodParticle* food = NULL; //the next particle of food to be gobbled up by the snake
string charHead = "O";
string charClear = " ";
string charFood = "X";
string charFieldTopBottom = "-";
string charFieldLeftRight = "|";

void draw_field()
{
    for (int x = TERMINAL_OFFSET_X; x < field_width + 1; x++)
    {
        for (int y = TERMINAL_OFFSET_Y; y < field_height + 1; y++)
        {
            printToCoordinates(x, y, charClear.c_str());
        }
    }
    //draw top
    for (int i = TERMINAL_OFFSET_Y; i < field_width + 1; i++)
    {
        printToCoordinates(i, 0, charFieldTopBottom.c_str());
    }
    //draw bottom
    for (int i = TERMINAL_OFFSET_Y; i < field_width + 1; i++)
    {
        printToCoordinates(i, field_height + 1, charFieldTopBottom.c_str());
    }
    //draw left side
    for (int i = TERMINAL_OFFSET_X; i < field_height + 2; i++)
    {
        printToCoordinates(0, i, charFieldLeftRight.c_str());
    }
    //draw right side
    for (int i = TERMINAL_OFFSET_X; i < field_height + 2; i++)
    {
        printToCoordinates(field_width + 1, i, charFieldLeftRight.c_str());
    }
}
void set_direction()
{
    if (key != prevkey)
    {
        if ((dir != Direction::Up && dir != Direction::Down) && (key == UP || key == W))
        {
            dir = Direction::Up;
        }
        else if ((dir != Direction::Down && dir != Direction::Up) && (key == DN || key == S))
        {
            dir = Direction::Down;
        }
        else if ((dir != Direction::Left && dir != Direction::Right) && (key == LF || key == A))
        {
            dir = Direction::Left;
        }
        else if ((dir != Direction::Right && dir != Direction::Left) && (key == RT || key == D))
        {
            dir = Direction::Right;
        }
        prevkey = key;
    }
}
void game_over()
{
    in_play = false;
    boom = true;
}
void new_game()
{
    in_play = true;
    if (snake != NULL)
    {
        snake->Destroy(&snake);
    }
    snake = NULL;
    snake->append(&snake, 1 + TERMINAL_OFFSET_X, 1 + TERMINAL_OFFSET_Y);
    snake_head = snake;
    snake_tail = snake;
    draw_field();
    dir = Direction::Right;
    growth_remaining = 4;
    food_nutrition = 4;
    moarFood = true;
}
void place_food(int X, int Y, int nutrition)
{
    if (food == nullptr)
    {
        food = (struct FoodParticle*)malloc(sizeof(struct FoodParticle));
    }
    food->X = X;
    food->Y = Y;
    food->nutrition = nutrition;
}
void moar_food(){
    int x = 0;
    int y = 0;
    while(true)
    {
        x = (rand() % field_width) + 1;
        y = (rand() % field_height) + 1;

        bool terminal_offset_check = true;

        if (x < FIELD_OFFSET_X1 || x > FIELD_OFFSET_X + field_width)
            terminal_offset_check = false;

        if (y < FIELD_OFFSET_Y1 || y > field_height - FIELD_OFFSET_Y2)
            terminal_offset_check = false;

        if (terminal_offset_check && !snake->HitTest(&snake, x, y, true))
            break;
    }
    place_food(x, y, food_nutrition);
    moarFood = false;
    drawFood = true;
}
void newPosition(struct BodySegment** head_ref)
{
    struct BodySegment* head = *head_ref;
    head = head->next;
    snake = head;
    if (head->HitTest(&head, head->X, head->Y, false))
    {
        game_over();
    }
    else if (food != nullptr)
    {
        if (head->HitTest(&head, food->X, food->Y, true))
        {
            if (sound)
                cout << '\a';
            growth_remaining += food->nutrition;
            moarFood = true;
        }
    }
    if (moarFood)
    {
        moar_food();
    }
}
void one_move()
{
    struct BodySegment* head = snake->GetHead(&snake);
    struct BodySegment* tail = snake->GetTail(&snake);

    if (head == nullptr || tail == nullptr)
        return;

    int xMov = 0;
    int yMov = 0;

    if (dir == Direction::Right)
    {
        if (head->X == field_width)
        {
            //struck the right wall
            game_over();
            return;
        }
        else
        {
            xMov = 1;
        }
    }
    if (dir == Direction::Left)
    {
        if (head->X - 1 < 1 + FIELD_OFFSET_X)
        {
            //struck the left wall
            game_over();
            return;
        }
        else
        {
            xMov = -1;
        }
    }
    if (dir == Direction::Up)
    {
        if (head->Y - 1 == 0 + FIELD_OFFSET_Y)
        {
            //struck the top wall
            game_over();
            return;
        }
        else
        {
            yMov = -1;
        }
    }
    if (dir == Direction::Down)
    {
        if (head->Y + 1 > field_height - FIELD_OFFSET_Y2)
        {
            //struck the bottom wall
            game_over();
            return;
        }
        else
        {
            yMov = 1;
        }
    }

    if (xMov != 0 || yMov != 0)
    {
        if (xMov != 0)
        {
            head->append(&head, head->X + xMov, head->Y);
        }
        else if (yMov != 0)
        {
            head->append(&head, head->X, head->Y + yMov);
        }
        newPosition(&head);
    }

    if (growth_remaining < 1)
    {
        snake = snake->DestroyTail(&tail);
    }
    else
    {
        growth_remaining--;
    }
}
void one_frame()
{
    struct BodySegment* head = snake->GetHead(&snake);
    struct BodySegment* tail = snake->GetTail(&snake);

    if (head == nullptr || tail == nullptr)
        return;

    if (snake_head != head)
    {
        //move head
        printToCoordinates(head->X, head->Y, "%s", charHead.c_str());
        snake_head = head;
    }
    if (snake_tail != tail)
    {
        //move tail
        printToCoordinates(tail->X, tail->Y, "%s", charClear.c_str());
        snake_tail = tail;
    }
    if (drawFood)
    {
        printToCoordinates(food->X, food->Y, "%s", charFood.c_str());
        drawFood = false;
    }
}
void show_help(){

    printToCoordinates(TERMINAL_OFFSET_X + 1, TERMINAL_OFFSET_Y + 1, "%s", "SNAKE!  Eat the food particles ( X ) to grow!");
    printToCoordinates(TERMINAL_OFFSET_X + 1, TERMINAL_OFFSET_Y + 2, "%s", "F5:       Enable/Disable sound");
    printToCoordinates(TERMINAL_OFFSET_X + 1, TERMINAL_OFFSET_Y + 3, "%s", "Spacebar: Start Game");
}
void kaboom()
{
    cout << endl << "GAME OVER!   Press SPACEBAR for a new snake." << endl;
    show_help();
    boom = false;
}

int main()
{
    hidecursor();
    draw_field();
    show_help();
    //game loop
    while(true)
    {
        //input
        int maybeKey = -1;
        maybeKey = get_input();
        if (maybeKey > -1)
            key = maybeKey;

        //process
        nsleep(speed);
//        nsleep(1);
//        moar_food();
//        printToCoordinates(food->X, food->Y, "%s", charFood.c_str());
//        continue;
        if (in_play)
        {
            set_direction();
            one_move();
            //output
            one_frame();
            key = -1;
        }
        else if (boom)
        {
            kaboom();
        }
        else if (key == SB)
        {
            new_game();
            key = -1;
        }
        else if (key == F5)
        {
            sound = !sound;
            printToCoordinates(1, field_height + 1, "%s%i", "Sound: ", sound);
            //cout << sound;
            key = -1;
        }
    }
    return 0;
}





















