#include <iostream>
#include "BodySegment.h"

using namespace std;

/* Given a reference (pointer to pointer) to the head of a list
and an int, inserts a new node on the front of the list. */
void BodySegment::push(struct BodySegment** head_ref, int X, int Y)
{
    /* 1. allocate node */
    struct BodySegment* new_node = (struct BodySegment*)malloc(sizeof(struct BodySegment));

    /* 2. put in the data  */
    new_node->X = X;
    new_node->Y = Y;

    /* 3. Make next of new node as head and previous as nullptr */
    new_node->next = (*head_ref);
    new_node->prev = nullptr;

    /* 4. change prev of head node to new node */
    if ((*head_ref) != nullptr)
        (*head_ref)->prev = new_node;

    /* 5. move the head to point to the new node */
    (*head_ref) = new_node;
}
/* Given a node as prev_node, insert a new node after the given node */
void BodySegment::insertAfter(struct BodySegment* prev_node, int X, int Y)
{
    /*1. check if the given prev_node is nullptr */
    if (prev_node == nullptr)
    {
        printf("the given previous node cannot be nullptr");
        return;
    }

    /* 2. allocate new node */
    struct BodySegment* new_node = (struct BodySegment*)malloc(sizeof(struct BodySegment));

    /* 3. put in the data  */
    new_node->X = X;
    new_node->Y = Y;

    /* 4. Make next of new node as next of prev_node */
    new_node->next = prev_node->next;

    /* 5. Make the next of prev_node as new_node */
    prev_node->next = new_node;

    /* 6. Make prev_node as previous of new_node */
    new_node->prev = prev_node;

    /* 7. Change previous of new_node's next node */
    if (new_node->next != nullptr)
        new_node->next->prev = new_node;
}
/* Given a reference (pointer to pointer) to the head
   of a DLL and an int, appends a new node at the end  */
void BodySegment::append(struct BodySegment** head_ref, int X, int Y)
{
    /* 1. allocate node */
    struct BodySegment* new_node = (struct BodySegment*)malloc(sizeof(struct BodySegment));

    struct BodySegment* last = *head_ref; /* used in step 5*/

    /* 2. put in the data  */
    new_node->X = X;
    new_node->Y = Y;

    /* 3. This new node is going to be the last node, so
          make next of it as nullptr*/
    new_node->next = nullptr;

    /* 4. If the Linked List is empty, then make the new
          node as head */
    if (*head_ref == nullptr)
    {
        new_node->prev = nullptr;
        *head_ref = new_node;
        return;
    }

    /* 5. Else traverse till the last node */
    while (last->next != nullptr)
        last = last->next;

    /* 6. Change the next of last node */
    last->next = new_node;

    /* 7. Make last node as previous of new node */
    new_node->prev = last;

    return;
}
void BodySegment::Destroy(struct BodySegment** head_ref)
{
    struct BodySegment* last = *head_ref; /* used in step 5*/
    struct BodySegment* prev = nullptr;

    //find the last seg
    while (last->next != nullptr)
    {
        last = last->next;
    }
    //delete all segs
    do
    {
        prev = last;
        last = last->prev;
        free(prev);
        prev = nullptr;
        if (last != nullptr)
            last->next = nullptr;
    }
    while (last != nullptr && last->prev != nullptr);
    free(last);
    last = nullptr;
}
BodySegment* BodySegment::DestroyTail(struct BodySegment** head_ref)
{
    struct BodySegment* tail = *head_ref; /* used in step 5*/
    struct BodySegment* next = *head_ref; /* used in step 5*/

    if (tail == nullptr)
        return nullptr;

    //find the last seg
    while (tail->prev != nullptr)
    {
        tail = tail->prev;
    }
    if (tail->next != nullptr)
    {
        tail->next->prev = nullptr;
        next = tail->next;
        free(tail);
        tail = nullptr;
        return next;
    }
    else
    {
        free(tail);
        tail = nullptr;
        return nullptr;
    }
}
BodySegment* BodySegment::GetTail(struct BodySegment** head_ref)
{
    struct BodySegment* tail = *head_ref; /* used in step 5*/

    if (tail == nullptr)
        return nullptr;

    if (*head_ref == nullptr)
        return nullptr;

    //find the last seg
    while (tail->prev != nullptr)
    {
        tail = tail->prev;
    }
    return tail;
}
BodySegment* BodySegment::GetHead(struct BodySegment** head_ref)
{
    struct BodySegment* head = *head_ref; /* used in step 5*/

    if (head == nullptr)
        return nullptr;

    if (*head_ref == nullptr)
        return nullptr;

    //find the last seg
    while (head->next != nullptr)
    {
        head = head->next;
    }
    return head;
}
bool BodySegment::HitTest(struct BodySegment** head_ref, int X, int Y, bool self)
{
    if (*head_ref == nullptr)
        return false;

    struct BodySegment* seg = this->GetHead(head_ref);

    if (seg == nullptr)
        return false;


    if (self && seg->X == X && seg->Y == Y)
        return true;

    while (seg->prev != nullptr)
    {
        if (seg->prev->X == X && seg->prev->Y == Y)
            return true;
        seg = seg->prev;
    }
    return false;
}
