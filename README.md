# Snake

SNAKE!  Eat the food particles ( X ) to grow!  Snake is a console game for windows and Linux variants.

# Building

Recommend use of Code::Blocks as a C++ IDE for building Snake, but regular GCC will work as well without it.

## Windows

To build with windows for windows include the preprocessor definition WINDOWS

## Linux

To build with linux for linux no preprocessor definition is needed.

# PLAY SNAKE ONLINE!!!

SSH to fartwhif.myqnapcloud.com port 57160
user gamer
password gamer